/*
 * Students.h
 *
 *  Created on: 16-Apr-2020
 *      Author: sunbeam
 */
#include<iostream>
#include<string>
#include<string.h>
#include<fstream>
#include<sstream>
#include<vector>
#include <string>

#include "preferences.h"
#ifndef STUDENTS_H_
#define STUDENTS_H_

using namespace std;
class Students
{
private:
	int form_no;
	string name;
	int rank_a;
	int rank_b;
	int rank_c;
	string degree;
	float degree_marks;
	string course_name;
	string center_id;
	string payment;
	int reported;
	string prn;
public:
	vector<preferences> Preferences;

public:
	Students();
	Students(int form_no,string name,int rank_a,int rank_b, int rank_c,string degree,float degree_marks,
			string course_name,string center_id,string payment,int reported,string prn);

	void set_form_no(int form_no);
	void set_name(string name);
	void set_rank_a(int rank_a);
	void set_rank_b(int rank_b);
	void set_rank_c(int rank_c);
	void set_degree(string degree);
	void set_degree_marks(float degree_marks);
	void set_course_name(string course_name);
	void set_center_id(string center_id);
	void set_payment(string payment);
	void set_reported(int reported);
	void set_prn(string prn);

	////////getters functions///////////////

	int get_form_no();
	string get_name();
	int get_rank_a();
	int get_rank_b();
	int get_rank_c();
	string get_degree();
	float get_degree_marks();
	string get_course_name();
	string get_center_id();
	string get_payment();
	int get_reported();
	string get_prn();
	void display();
    void display_preferences();


};





#endif /* STUDENTS_H_ */
