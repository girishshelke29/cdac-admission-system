

#include<iostream>
#include<string>
#include<vector>

#include "preferences.h"

#include "Students.h"


using namespace std;


Students::Students()
{
		form_no=0;
		name="";
		rank_a=0;
		rank_b=0;
		rank_c=0;
		degree="";
		degree_marks=0.0f;
		course_name="";
		center_id="";
		payment="";
		reported=0;
		prn="";
}
Students::Students(int form_no,string name,int rank_a,int rank_b, int rank_c,string degree,float degree_marks,
			string course_name,string center_id,string payment,int reported,string prn)
	{
				this->form_no=form_no;
				this->name=name;
				this->rank_a=rank_a;
				this->rank_b=rank_b;
				this->rank_c=rank_c;
				this->degree=degree;
				this->degree_marks=degree_marks;
				this->course_name=course_name;
				this->center_id=center_id;
				this->payment=payment;
				this->reported=reported;
				this->prn=prn;
	}
	void Students::set_form_no(int form_no)
	{
		this->form_no=form_no;
	}

	void Students::set_name(string name)
	{
		this->name=name;
	}

	void Students::set_rank_a(int rank_a)
	{
		this->rank_a=rank_a;
	}

	void Students::set_rank_b(int rank_b)
	{
		this->rank_b=rank_b;
	}

	void Students::set_rank_c(int rank_c)
	{
		this->rank_c=rank_c;
	}

	void Students::set_degree(string degree)
	{
		this->degree=degree;
	}

	void Students::set_degree_marks(float degree_marks)
	{
		this->degree_marks=degree_marks;
	}

	void Students::set_course_name(string course_name)
	{
		this->course_name=course_name;
	}

	void Students::set_center_id(string center_id)
	{
		this->center_id=center_id;
	}

	void Students::set_payment(string payment)
	{
		this->payment=payment;
	}

	void Students::set_reported(int reported)
	{
		this->reported=reported;
	}

	void Students::set_prn(string prn)
	{
		this->prn=prn;
	}

////////getters functions///////////////

int Students::get_form_no()
{
	return this->form_no;
}
string Students::get_name()
{
	return this->name;
}
int Students::get_rank_a()
{
	return this->rank_a;
}
int Students::get_rank_b()
{
	return this->rank_b;
}
int Students::get_rank_c()
{
	return this->rank_c;
}
string Students::get_degree()
{
	return this->degree;
}
float Students::get_degree_marks()
{
	return this->degree_marks;
}
string Students::get_course_name()
{
	return this->course_name;
}
string Students::get_center_id()
{
	return this->center_id;
}
string Students::get_payment()
{
	return this->payment;
}
int Students::get_reported()
{
	return this->reported;
}
string Students::get_prn()
{
	return this->prn;
}

void Students::display()
{
	cout<<"id: "<<this->form_no<<endl;
	cout<<"name: "<<this->name<<endl;
	cout<<"rank in a: "<<this->rank_a<<endl;
	cout<<"rank in b: "<<this->rank_b<<endl;
	cout<<"rank in c: "<<this->rank_c<<endl;
	cout<<"degree: "<<this->degree<<endl;
	cout<<"degree_marks:"<<this->degree_marks<<endl;
	cout<<"course_name: "<<this->course_name<<endl;
	cout<<"centre id: "<<this->center_id<<endl;
	cout<<"payement status: "<<this->payment<<endl;
	cout<<"reported: "<<this->reported<<endl;
	cout<<"PRN: "<<this->prn<<endl;

}

void Students::display_preferences()
{
	for(unsigned int i=0;i<Preferences.size();i++)
	{
		Preferences[i].display();
	}

}

