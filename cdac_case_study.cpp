//============================================================================
// Name        : cdac_case_study.cpp
// Author      : girish
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================



#include<iostream>
#include<string>
#include<string.h>
#include<fstream>
#include<sstream>
#include<vector>
#include "Students.h"
#include "preferences.h"
using namespace std;



void insert_Students(vector<Students>& Student)
{
	ifstream fp;
	string line;
	int c;
	fp.open("Students.csv");
	if(!fp)
	{
		cout<<"failed to open Students file"<<endl;
		return;
	}
	c=0;
	while(getline(fp,line))
	{
		stringstream str(line);
		string tokens[12];
		for(int i=0;i<12;i++)
		{
			getline(str,tokens[i],',');
		}


		Students b(std::stoi(tokens[0]),tokens[1],std::stoi(tokens[2]),std::stoi(tokens[3]),std::stoi(tokens[4]),tokens[5],std::stof(tokens[6]),tokens[7],tokens[8],tokens[9],std::stoi(tokens[10]),tokens[11]);
		Student.push_back(b);
		c++;
	}
	fp.close();
	cout<<" inserted successfully"<<c<<endl;
}




Students* find_student(vector<Students>& Student,int form_no1)
{
	unsigned i;
	for(i=0;i<Student.size();i++)
	{


		if(Student[i].get_form_no()==form_no1)
		{
			return &Student[i];
		}

	}
	return NULL;
}

void load_preference(vector<Students>& Student)
{
	ifstream fp;
	string line;
	int c;
	fp.open("preferences.csv");
	if(!fp)
	{
		cout<<"failed to open Students file"<<endl;
		return;
	}
	c=0;
	while(getline(fp,line))
	{
		stringstream str(line);
		string tokens[4];
		for(int i=0;i<4;i++)
		{
			getline(str,tokens[i],',');
		}


		preferences p(stoi(tokens[0]),std::stoi(tokens[1]),tokens[2],tokens[3]);
		Students *s =find_student(Student,p.get_form_no());
		s->Preferences.push_back(p);
		c++;
	}
	fp.close();
	cout<<" inserted successfully"<<c<<endl;
}




int main()
{
	vector<Students>student;
	insert_Students(student);
	load_preference(student);
	cout<<"records are"<<endl;
	for(unsigned int i=0;i<student.size();i++)
	{

		student[i].display();
		student[i].display_preferences();

		cout<<endl;
	}


return 0;
}





