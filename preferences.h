/*
 * preferences.h
 *
 *  Created on: 17-Apr-2020
 *      Author: sunbeam
 */
#include<iostream>
#include<string>
#include<string.h>
#include<fstream>
#include<sstream>
#include<vector>


#ifndef PREFERENCES_H_
#define PREFERENCES_H_


using namespace std;

//std::string::size_type sz;

class preferences
{
private:
	int form_no;
	int preference_no;
	string name;
	string center_id;

public:
	preferences();
	preferences(int form_no,int preference_no,string name,string center_id);

	void set_form_no(int form_no);
	void set_preference_no(int preference_no);
	void set_name(string name);
	void set_center_id(string center_id);
	int get_form_no();
	int get_preference_no();
	string get_name();
	string get_center_id();
	void display();
	//void set_name(string name);
};




#endif /* PREFERENCES_H_ */
