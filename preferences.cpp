/*
 * preferences.cpp
 *
 *  Created on: 17-Apr-2020
 *      Author: sunbeam
 */

#include<iostream>
#include<string>
#include<string.h>
#include<fstream>
#include<sstream>
#include<vector>

#include "preferences.h"
using namespace std;


preferences::preferences()
{
		this->form_no=0;
		this->preference_no=0;
		this->name="";
		this->center_id="";
}
preferences::preferences(int form_no,int preference_no,string name,string center_id)
{
		this->form_no=form_no;
		this->preference_no=preference_no;
		this->name=name;
		this->center_id=center_id;
}

	void preferences::set_form_no(int form_no)
	{
		this->form_no=form_no;
	}
	void preferences::set_preference_no(int preference_no)
	{
		this->preference_no=preference_no;

	}
	void preferences::set_name(string name)
	{
		this->name=name;
	}
	void preferences::set_center_id(string center_id)
	{
		this->center_id=center_id;
	}

	int preferences::get_form_no()
	{
		return this->form_no;
	}
	int preferences::get_preference_no()
	{
		return this->preference_no;
	}
	string preferences::get_name()
	{
		return this->name;
	}
	string preferences::get_center_id()
	{
		return this->center_id;
	}
	void preferences::display()
	{
		cout<<this->form_no<<","<<this->preference_no<<","<<this->name<<","<<this->center_id;
		cout<<endl;
	}
